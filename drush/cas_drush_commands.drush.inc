<?php

/**
 * @file
 * Drush 8 commands for CAS.
 */

/**
 * Implements hook_drush_command().
 */
function cas_drush_commands_drush_command() {
  $items = [];

  $items['cas-user-create'] = [
    'callback' => 'cas_drush_commands_cas_user_create',
    'description' => 'Create a CAs user account with the specified CAs username.',
    'arguments' => [
      'cas_username' => 'The CAS username of the account to add.',
      'mail' => '(optional) Email to set for the account.',
      'roles' => '(optional) A comma delimited list of roles to add to the account.'
    ],
    'required-arguments' => 1,
  ];

  $items['cas-set-username'] = [
    'callback' => 'cas_drush_commands_cas_set_username',
    'description' => 'Set the CAS username for the given user',
    'arguments' => [
      'drupal_username' => 'The name of the existing Drupal user.',
      'cas_username' => 'The CAS username to add.',
    ],
    'required-arguments' => TRUE,
  ];

  return $items;
}

function cas_drush_commands_cas_user_create($cas_username, $mail = '', $roles = '') {
  $account = user_load_by_name($cas_username);

  if (!$account && !drush_get_context('DRUSH_SIMULATE')) {
    $cas_user_manager = \Drupal::service('cas.user_manager');
    $property_values = ['cas' => $cas_username];

    if ($mail) {
      $property_values['mail'] = $mail;
    }

    if ($roles) {
      $property_values['roles'] = explode(',', $roles);
    }

    $user = $cas_user_manager->register($cas_username, $property_values, $cas_username);

    if ($user) {
      $fields = ['cas', 'uid', 'name', 'mail', 'roles', 'status'];
      $user = $user->toArray();
      $user = array_intersect_key($user, array_flip($fields));
      $user = drush_key_value_to_array_table($user);

      drush_print_table($user);
    }
    else {
      drush_set_error(dt('Could not create a new user account with CAs username @cas_username.', [
        '@cas_username' => $cas_username
      ]));
    }
  }
  else {
    drush_set_error(dt('There is already a user account with CAS username @cas_username.', [
      '@cas_username' => $cas_username
    ]));
  }
}

function cas_drush_commands_cas_set_username($drupal_username, $cas_username) {
  $account = user_load_by_name($drupal_username);

  if ($account && !drush_get_context('DRUSH_SIMULATE')) {
    $cas_user_manager = \Drupal::service('cas.user_manager');
    $cas_user_manager->setCasUsernameForAccount($account, $cas_username);

    drush_log(dt('Assiged CAS username "!cas_username" to user "!drupal_username"', [
      '!cas_username' => $cas_username,
      '!drupal_username' => $drupal_username,
    ]));
  }
  else {
    drush_set_error(dt('Unable to load user: !user', [
      '!user' => $drupal_username,
    ]));
  }
}
